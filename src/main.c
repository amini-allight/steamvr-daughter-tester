/*
Copyright 2024 Amini Allight

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#if __STDC_VERSION__ >= 199901L
#define _XOPEN_SOURCE 600
#else
#define _XOPEN_SOURCE 500
#endif

#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <stdio.h>
#include <wiringPi.h>

#define CLK_PIN 24
#define DTA_PIN 23

static const int config[] = {
    0, 0,           /* TIA gain control */
    0, 1,           /* TIA BW control */
    0,              /* Data gain control */
    0, 1,           /* Data slicer threshold */
    0,              /* Envelope gain control */
    0, 1,           /* Envelope slicer threshold */
    1,              /* Envelope detector attack time */
    0,              /* Envelope low pass filter BW setting */
    0, 1            /* MODE */
};
static const int configBitCount = sizeof(config) / sizeof(int);

static void microsleep(long us)
{
    struct timespec ts;
    ts.tv_sec = 0;
    ts.tv_nsec = us * 1000;
    nanosleep(&ts, NULL);
}

static long currentMicroseconds()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);

    return 1000000 * tv.tv_sec + tv.tv_usec;
}

void enterConfigMode()
{
    pinMode(CLK_PIN, OUTPUT);
    pinMode(DTA_PIN, OUTPUT);

    microsleep(1);
    digitalWrite(CLK_PIN, LOW);
    microsleep(1);
    digitalWrite(DTA_PIN, HIGH);
    microsleep(1);
    digitalWrite(CLK_PIN, HIGH);
    microsleep(1);
    digitalWrite(DTA_PIN, LOW);
    microsleep(1);
}

void exitConfigMode()
{
    pinMode(CLK_PIN, OUTPUT);
    pinMode(DTA_PIN, OUTPUT);

    microsleep(1);
    digitalWrite(DTA_PIN, HIGH);
    microsleep(1);
    digitalWrite(CLK_PIN, LOW);
    microsleep(1);
    digitalWrite(DTA_PIN, LOW);
    microsleep(1);
    digitalWrite(CLK_PIN, HIGH);
    microsleep(1);
}

void writeConfig()
{
    pinMode(CLK_PIN, OUTPUT);
    pinMode(DTA_PIN, OUTPUT);

    /* Initial state */
    digitalWrite(CLK_PIN, HIGH);
    digitalWrite(DTA_PIN, HIGH);
    sleep(1);

    /* Config begin */
    digitalWrite(CLK_PIN, HIGH);
    digitalWrite(DTA_PIN, LOW);
    microsleep(1);

    /* Start write bit */
    digitalWrite(CLK_PIN, LOW);
    digitalWrite(DTA_PIN, LOW);
    microsleep(1);

    digitalWrite(CLK_PIN, HIGH);
    microsleep(1);

    /* Write config */
    int i;
    for (i = 0; i < configBitCount; i++)
    {
        digitalWrite(CLK_PIN, LOW);
        digitalWrite(DTA_PIN, config[i]);
        microsleep(1);

        digitalWrite(CLK_PIN, HIGH);
        microsleep(1);
    }

    /* End write bit */
    digitalWrite(CLK_PIN, LOW);
    digitalWrite(DTA_PIN, LOW);
    microsleep(1);

    digitalWrite(CLK_PIN, HIGH);
    microsleep(1);
}

void readConfig()
{
    pinMode(CLK_PIN, OUTPUT);
    pinMode(DTA_PIN, OUTPUT);

    /* Initial state */
    digitalWrite(CLK_PIN, HIGH);
    digitalWrite(DTA_PIN, HIGH);
    sleep(1);

    /* Config begin */
    digitalWrite(CLK_PIN, HIGH);
    digitalWrite(DTA_PIN, LOW);
    microsleep(1);

    /* Start read bit */
    digitalWrite(CLK_PIN, LOW);
    digitalWrite(DTA_PIN, HIGH);
    microsleep(1);

    digitalWrite(CLK_PIN, HIGH);
    microsleep(1);

    /* Switch mode */
    pinMode(DTA_PIN, INPUT);

    /* Read config */
    int i;
    for (i = 0; i < configBitCount; i++)
    {
        digitalWrite(CLK_PIN, LOW);
        microsleep(1);

        digitalWrite(CLK_PIN, HIGH);
        microsleep(1);

        printf("%i: %i\n", i, digitalRead(DTA_PIN));
    }

    /* Switch mode */
    pinMode(DTA_PIN, OUTPUT);

    /* End read bit */
    digitalWrite(CLK_PIN, LOW);
    digitalWrite(DTA_PIN, LOW);
    microsleep(1);

    digitalWrite(CLK_PIN, HIGH);
    microsleep(1);
}

void listen()
{
    pinMode(CLK_PIN, INPUT);
    pinMode(DTA_PIN, INPUT);

    int envelope = 0;
    long envelopeStartTime = 0;

    while (1)
    {
        int clk = digitalRead(CLK_PIN);

        if (!clk && !envelope)
        {
            envelope = 1;
            envelopeStartTime = currentMicroseconds();
        }
        else if (clk && envelope)
        {
            printf("Got envelope of %li μs\n", currentMicroseconds() - envelopeStartTime);
            envelope = 0;
        }

        if (envelope)
        {
            int dta = digitalRead(DTA_PIN);

            printf("%i\n", dta);
        }
    }
}

int main(int argc, char** argv)
{
    wiringPiSetupGpio();
    pullUpDnControl(CLK_PIN, PUD_UP);
    pullUpDnControl(DTA_PIN, PUD_DOWN);

    sleep(1);

    enterConfigMode();

    writeConfig();

    readConfig();

    exitConfigMode();

    listen();

    return 0;
}
