cmake_minimum_required(VERSION 3.5)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

project(steamvr-daughter-tester)

add_definitions("-std=c89 -Wall -g")

file(GLOB TESTER_SOURCES "${PROJECT_SOURCE_DIR}/src/*.c")
add_executable(tester ${TESTER_SOURCES})
set_target_properties(tester PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/bin")

target_link_libraries(tester wiringPi)
