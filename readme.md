# SteamVR Daughter Tester

A Raspberry Pi based test utility for the TS4631 SteamVR IC.

## Usage

**Note:** This was tested with a SteamVR base station 2.0. I don't know how it will behave if exposed to a base station 1.0.

This is intended to be run on a Raspberry Pi with GPIO pins available and enabled. Two GPIO pins must be connected to the E and D pins of the TS4631 chip. The rest of the TS4631's pins must be connected to passive components, voltage sources, the photodiode used to receive light pulses from the SteamVR base station, etc. as described in the chip's datasheet.

Before compiling you must set the macros `CLK_PIN` and `DTA_PIN` in `src/main.c` to the pin numbers connected to the E and D wires respectively. **These must be BCM pin numbers, not wiringPi pin numbers or physical pin numbers.** You can get a table converting between the different types of pin numbers for your Raspberry Pi by running `gpio readall` on your Pi.

Example: I used a Raspberry Pi 3 Model B+ with the E wire connected to physical pin 18 and the D wire connected to physical pin 16. These correspond to BCM pins 24 and 23 respectively so those are the default values you'll find in the source code.

## Dependencies

- CMake, Make and a C compiler for building
- [WiringPi](https://github.com/WiringPi/WiringPi) for GPIO access

## Building

Make sure you have the dependencies installed then run these commands to build the program.

```sh
mkdir -p build
cd build
cmake ..
make
```

## Running

Run these commands to run the program.

```sh
cd bin
./tester
```

## Expected Output

Upon startup the program should print a series of binary values corresponding to the configuration written to the chip. If configured correctly this will be the output:

```
0: 0
1: 0
2: 0
3: 1
4: 0
5: 0
6: 1
7: 0
8: 0
9: 1
10: 1
11: 0
12: 0
13: 1
```

After the configuration has been printed placing the photodiode within view of a SteamVR base station should cause the program to print a series of 0s and 1s corresponding to the carrier data sent along with the pulse. This is followed by a message denoting the length of the pulse in microseconds.

## Credit & License

Developed by Amini Allight, licensed under the Apache License 2.0.

This tool is not affiliated with or endorsed by either Valve Corporation or Triad Semiconductor.
